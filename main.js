// const { character } = require('./Character')

// const axios = require('axios')

const cards = document.getElementsByClassName('cards')

window.onload() = function () {
	const url = 'https://rickandmortyapi.com/api/character';
	fetch(url)
		.then(res => res.json())
		.then(data => {
			const rawData = data.results;
			return rawData.map(character => {
				//all needed data is listed below as an entity 
				let created = character.created;
				let species = character.species;
				let img = character.image;
				let episodes = character.episode;
				let name = character.name;
				let location = character.location;
				let card = document.createElement('li');
				card = character;
				cards.appendChild(card);
			});
		})
		.catch((error) => {
			console.log(JSON.stringify(error));
		});
}

const data = [] // storage for data from JSON
const characters = [] // storage for sorted characters
const searchedCharacters = [] // storage for characters if we search

function getListContent() {
	let result = [];
	for (let i = 1; i <= 10; i++) {
		let card = document.createElement('li');
		card.append(i);
		result.push(card);
	}
	return result;
}
characters.append(getListContent());

// list episode creation
const renderEpisodes = (episodes) => {
	return episodes.map(episode => (
		<span>episode</span>
	)
	)
}

// card creation, we pass characters from data.result
const renderCard = (characters) => {
	return characters.map(item => {
		const { name, race, image, species, location, created, episode } = item
		return (
			<Card>
				<name>{name}</name>
				<race>{race}</race>
				<image>{image}</image>
				<species>{species}</species>
				<location>{location}</location>
				<created>{created}</created>
				<episodes>{renderEpisodes(episode)}</episodes>
			</Card>
		)
	})
}

// sort by creation date
const sortByCreationDate = () => {
	characters = characters.sort((first, second) => {
		return new Date(first.created) - new Date(second.created)
	})
}

// sort by amount of episodes
const sortByCreationDate = () => {
	characters = characters.sort((first, second) => {
		return first.episode.length - second.episode.length
	})
}

// delete character from the list by id
const deleteCharacter = (id) => {
	characters = characters.filter(character => character.id !== id)
}

// search characters
const searchCharacters = (search) => {
	searchedCharacters = characters.filter(character => character.name.toLowerCase().includes(search.toLowerCase()))
}

















